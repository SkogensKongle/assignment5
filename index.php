<?php
//Definitions
$dbname = "ass5";
$doc = new DOMDocument();
$doc->load('SkierLogs.xml');
$doc->normalize();
$xpath = new DOMXPath($doc);
$db = pdoConnect($dbname);
$Check = Check($db);

//.............................CONNECTION......................................
function pdoConnect($dbname)
{
    try {
        //Creating the PDO connection.
        $db = new PDO('mysql:host=localhost;dbname='.$dbname.';charset=utf8mb4', 'root', '');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

         //Error message.
    } catch (PDOException $ex) {
        echo "Could not connect to database";
    }
    return $db;
}

//..............................FUNCTIONS......................................



//..............................First and Last name............................
function getfirstName(DOMXPath $xpath){
    $firstnames = array();
    $query = "//SkierLogs/Skiers/Skier/FirstName";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $firstnames[] = $entry->nodeValue;
    }
    return $firstnames;
}
function getlastName(DOMXPath $xpath){
    $lastnames = array();
    $query = "//SkierLogs/Skiers/Skier/LastName";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $lastnames[] = $entry->nodeValue;
    }
    return $lastnames;
}

//.................................Get club values............................
function getClubId(DOMXPath $xpath, $season){
    $clubids = array();
    $query = "//Season[@fallYear='".$season."']/Skiers/@clubId";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $clubids[] = $entry->nodeValue;
    }
    return $clubids;
}
function getClubID(DOMXPath $xpath){
    $clubids = array();
    $query = "//SkierLogs/Clubs/Club/@id";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $clubids[] = $entry->nodeValue;
    }
    return $clubids;
}
function getClubNames(DOMXPath $xpath){
    $clubnames = array();
    $query = "//Clubs/Club/Name";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $clubnames[] = $entry->nodeValue;
    }
    return $clubnames;
}
function getClubCity(DOMXPath $xpath){
    $clubcity = array();
    $query = "//Clubs/Club/City";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $clubcity[] = $entry->nodeValue;
    }
    return $clubcity;
}
function getClubCounty(DOMXPath $xpath){
    $clubcounty = array();
    $query = "//Clubs/Club/County";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $clubcounty[] = $entry->nodeValue;
    }
    return $clubcounty;
}

//........................Get seasons.........................................
function getSeasons(DOMXPath $xpath){
    $seasons = array();
    $query = "//@fallYear";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $seasons[] = $entry->nodeValue;
    }
    return $seasons;
}
//.....................Two getfunctions for userName..........................
function getUsernames(DOMXPath $xpath, $season, $clubID){
    $usernames = array();
    $query = "//Season[@fallYear='".$season."']/Skiers[@clubId='".$clubID."']/Skier/@userName";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $usernames[] = $entry->nodeValue;
    }
    return $usernames;
}
function getUsername2(DOMXPath $xpath){
    $usernames = array();
    $query = "//SkierLogs/Skiers/Skier/@userName";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $usernames[] = $entry->nodeValue;
    }
    return $usernames;
}

//............................Get date of birth.............................
function getDOB(DOMXPath $xpath){
    $dob = array();
    $query = "//SkierLogs/Skiers/Skier/YearOfBirth";
    $entries = $xpath->evaluate($query);
    foreach( $entries as $entry ){
        $dob[] = $entry->nodeValue;
    }
    return $dob;
}

//.............................Get total Distance.............................
function getTotalDist(DOMXPath $xpath, $season, $username, $clubID){
    $query = "sum(//Season[@fallYear='".$season."']/Skiers[@clubId='".$clubID."']/Skier[@userName='".$username."']//Distance)";
    $distanse = $xpath->evaluate($query);
    return $distanse;
}



//............................AddingFunctions..................................



function AddSeason($db, $season){
    try{
        $query = "INSERT INTO season (fallYear)  VALUES (?)";
        $param = array($season);
        $stmt = $db->prepare($query);
        $stmt->execute($param);
        //Error message
    } catch(PDOException $ex){
        echo "Error".$ex;
    }
}
function AddClubs($db, $clubID, $clubname, $clubcity, $clubcounty){
    try{
        $query = "INSERT INTO skierclub (id, Name, City, County)  VALUES (?, ?, ?, ?)";
        $param = array($clubID, $clubname, $clubcity, $clubcounty);
        $stmt = $db->prepare($query);
        $stmt->execute($param);
        //Error message
    } catch(PDOException $ex){
        echo "Error".$ex;
    }
}
function AddSkier($db, $username, $firstname, $lastname, $dob){
    try{
        $query = "INSERT INTO skier (userName, firstName, lastName, yearOfBirth)  VALUES (?, ?, ?, ?)";
        $param = array($username, $firstname, $lastname, $dob);
        $stmt = $db->prepare($query);
        $stmt->execute($param);
        //Error message
    } catch(PDOException $ex){
        echo "Error".$ex;
    }
}
function AddNone($db){
    try{
        $query = "INSERT INTO skierclub (id)  VALUES (?)";
        $param = array("none");
        $stmt = $db->prepare($query);
        $stmt->execute($param);
        //Error message
    } catch(PDOException $ex){
        echo "Error".$ex;
    }
}
function AddAssignment($db, $season, $clubID, $username, $totalDist){
    try{
        if ($clubID == ""){
            $clubID = "none";
        }

        $query = "INSERT INTO Assignment (userName, fallYear, id, Total)  VALUES (?, ?, ?, ?)";
        $param = array($username, $season, $clubID, $totalDist);
        $stmt = $db->prepare($query);
        $stmt->execute($param);
        //Error message
    } catch(PDOException $ex){
        echo "Error".$ex;
    }
}
function Check($db){
    try{

        $query = "SELECT * FROM Assignment, season, skierclub, skier LIMIT 2";
        $param = array();
        $stmt = $db->prepare($query);
        $stmt->execute($param);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (sizeof($row) > 1) return true;
        //Error message
    }catch(PDOException $ex){
        echo "Error ".$ex;
    }
    return false;
}

//..........................Where i put it all together........................
if ($Check == false){
    $seasons = getSeasons($xpath);
    foreach ($seasons as $season) {
        AddSeason($db, $season);
    }
    AddNone($db);
    $clubids2 = getClubID($xpath);
    $clubnames = getClubNames($xpath);
    $clubcity = getClubCity($xpath);
    $clubcounty = getClubCounty($xpath);
    for($x = 0; $x < sizeof($clubnames); $x++) {
        AddClubs($db, $clubids2[$x], $clubnames[$x], $clubcity[$x], $clubcounty[$x]);
    }
    $usernames2 = getUsername2($xpath);
    $firstnames = getfirstName($xpath);
    $lastnames = getlastName($xpath);
    $dob = getDOB($xpath);
    for($x = 0; $x < sizeof($usernames2); $x++) {
        AddSkier($db, $usernames2[$x], $firstnames[$x], $lastnames[$x], $dob[$x]);
    }
    foreach ($seasons as $season){
        $clubids = getClubId($xpath, $season);
        foreach ($clubids as $clubID){
            $usernames = getUsernames($xpath, $season, $clubID);
            foreach ($usernames as $username){
                $totalDist = getTotalDist($xpath, $season, $username, $clubID);

                AddAssignment($db, $season, $clubID, $username, $totalDist);
            }
        }
    }
    echo "Completed.";
}else{
    echo "...The database needs to be empty...";
}
?>
